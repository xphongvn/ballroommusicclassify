import madmom
import time
import os
from tqdm import tqdm, tqdm_gui
import numpy as np

tqdm.monitor_interval = 0

# start_time = time.time()
# proc = madmom.features.DBNDownBeatTrackingProcessor(beats_per_bar=[3, 4], fps=100)
# act = madmom.features.RNNDownBeatProcessor()('/Users/noemievoss/ballroommusicclassify/music_sample_wav/waltz/christinaperriathousandyears (1).mp3.wav')
# count = proc(act)
# print(count[:,1])
# end_time = time.time()
# print("{} seconds hae elapsed".format(end_time-start_time))
#
# start_time = time.time()
# proc = madmom.features.DBNDownBeatTrackingProcessor(beats_per_bar=[3, 4], fps=100)
# act = madmom.features.RNNDownBeatProcessor()('/Users/noemievoss/ballroommusicclassify/music_sample_wav/chachacha/02  Maroon 5 - Makes Me Wonder.mp3.wav')
# count = proc(act)
# print(count[:,1])
# end_time = time.time()
# print("{} seconds hae elapsed".format(end_time-start_time))

# Name of Song, Type of Dance, Count (3 or 4)

## Main program ##

# Open a csv and write the title
f = open('song_count_madmom.csv', mode = 'w')
f.write("Name of Song,Type of Dance,Count\n")

# Open all wav songs and calculate the bpm and write to the csv file
list_of_dir = os.listdir("../music_sample_wav/")
if ".DS_Store" in list_of_dir:
    list_of_dir.remove(".DS_Store")

for dir in tqdm(list_of_dir):
    folder_path = "../music_sample_wav/" + dir
    file_list = os.listdir(folder_path)
    for file in tqdm(file_list):
        file_path = folder_path + "/" + file
        try:
            proc = madmom.features.DBNDownBeatTrackingProcessor(beats_per_bar=[2, 3, 4], fps=100)
            act = madmom.features.RNNDownBeatProcessor()(file_path)
            count = proc(act)
            max_count = int(count[:,1].max())
            f.write(file + "," + dir + "," + str(max_count) + "\n")
            print("The Count of " + file + " is: " + str(np.unique(count[:,1])))
        except:
            print("error in file:",file)

f.close()