import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt


# read the song_features file
fn = "song_features_advanced.csv"
df = pd.read_csv(fn)

# convert types of dance into unique integer numbers in order to classify
targets = df["Type of Dance"].unique()
map_to_integer = {dance:n for n, dance in enumerate(targets)}
map_to_labels = {n:dance for n, dance in enumerate(targets)}
targets_column = df["Type of Dance"].replace(map_to_integer)

# subset the features data
features = list(df.columns[2:9])

# split data into train and test data
X_train, X_test, y_train, y_test = train_test_split(df[features], targets_column, test_size=.2, random_state=100)

# build the model
model = RandomForestClassifier(n_estimators=1000)
model.fit(X_train, y_train)

# evaluate the model
y_pred = model.predict(X_test)
# convert y_pred and y_test back into labels
y_test_labels = y_test.replace(map_to_labels)
y_pred_labels = pd.Series(y_pred).replace(map_to_labels)

score = metrics.classification_report(y_test_labels, y_pred_labels)
print(score)


plt.figure()
mat = metrics.confusion_matrix(y_test_labels, y_pred_labels)
sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False,
            xticklabels=targets, yticklabels=targets)
plt.xlabel('true label')
plt.ylabel('predicted label')
plt.title('Result of Random Forest Model (trees = 1000)')
plt.show()