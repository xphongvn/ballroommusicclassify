from pydub import AudioSegment
import os

list_of_dir = os.listdir("../music_sample_mp3")
list_of_dir.remove(".DS_Store")

check_dir = "../music_sample_wav/"
if not os.path.isdir(check_dir):
    os.mkdir(check_dir)

for dir in list_of_dir:
    check_dir = "../music_sample_wav/" + dir
    if not os.path.isdir(check_dir):
        os.mkdir(check_dir)

for dir in list_of_dir:
    folder_to_convert = "../music_sample_mp3/" + dir
    files_to_convert = os.listdir(folder_to_convert)
    folder_to_save = "../music_sample_wav/" + dir
    for filename in files_to_convert:
        try:
            sound = AudioSegment.from_mp3(folder_to_convert + "/" + filename)
            sound.export(folder_to_save + "/" + filename + ".wav", format="wav")
        except:
            print("Error in " + filename)