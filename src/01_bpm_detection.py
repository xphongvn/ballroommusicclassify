import wave, array, math, time, argparse, sys
import numpy, pywt
from scipy import signal
import os
from tqdm import tqdm
from collections import Counter

tqdm.monitor_interval = 0

# Read the original WAV file into numpy
def read_wav(filename):
    # open file, get metadata for audio
    try:
        wf = wave.open(filename, 'rb')
    except OSError as err:
        print("there is an error:" + err)
        return

    # typ = choose_type( wf.getsampwidth() ) #TODO: implement choose_type
    nsamps = wf.getnframes();
    assert (nsamps > 0);

    fs = wf.getframerate()
    assert (fs > 0)

    # read entire file and make into an array
    samps = list(array.array('i', wf.readframes(nsamps)))
    # print 'Read', nsamps,'samples from', filename
    try:
        assert (nsamps == len(samps))
    except AssertionError as e:
        print(nsamps, "not equal to", len(samps), "in song: ", filename)
        return None, None

    return samps, fs


# print an error when no data can be found
def no_audio_data():
    #print("No audio data for sample, skipping...")
    return None, None


# simple peak detection
def peak_detect(data):
    max_val = numpy.amax(abs(data))
    peak_ndx = numpy.where(data == max_val)
    if len(peak_ndx[0]) == 0:  # if nothing found then the max must be negative
        peak_ndx = numpy.where(data == -max_val)
    return peak_ndx


def bpm_detector(data, fs):
    cA = []
    cD = []
    correl = []
    cD_sum = []
    levels = 4
    max_decimation = 2 ** (levels - 1);
    min_ndx = 60. / 220 * (fs / max_decimation)
    max_ndx = 60. / 40 * (fs / max_decimation)

    for loop in range(0, levels):
        cD = []
        # 1) DWT
        if loop == 0:
            [cA, cD] = pywt.dwt(data, 'db4')
            cD_minlen = int(len(cD) / max_decimation + 1)
            cD_sum = numpy.zeros(cD_minlen)
        else:
            [cA, cD] = pywt.dwt(cA, 'db4');
        # 2) Filter
        cD = signal.lfilter([0.01], [1 - 0.99], cD)

        # 4) Subtractargs.filename out the mean.

        # 5) Decimate for reconstruction later.
        cD = abs(cD[::(2 ** (levels - loop - 1))]);
        cD = cD - numpy.mean(cD)
        # 6) Recombine the signal before ACF
        #    essentially, each level I concatenate
        #    the detail coefs (i.e. the HPF values)
        #    to the beginning of the array
        cD_sum = cD[0:cD_minlen] + cD_sum;

    if [b for b in cA if b != 0.0] == []:
        return no_audio_data()
    # adding in the approximate data as well...
    cA = signal.lfilter([0.01], [1 - 0.99], cA)
    cA = abs(cA)
    cA = cA - numpy.mean(cA)
    cD_sum = cA[0:cD_minlen] + cD_sum;

    # ACF
    correl = numpy.correlate(cD_sum, cD_sum, 'full')

    midpoint = int(len(correl) / 2)
    correl_midpoint_tmp = correl[midpoint:]
    peak_ndx = peak_detect(correl_midpoint_tmp[int(min_ndx):int(max_ndx)])
    if len(peak_ndx) > 1:
        return no_audio_data()

    peak_ndx_adjusted = peak_ndx[0] + min_ndx;
    bpm = 60. / peak_ndx_adjusted * (fs / max_decimation)
    #print(bpm)
    return bpm, correl


# Calculate the whole song bpm
def calculate_bpm_single_window(fn, window):
    samps, fs = read_wav(fn)
    if samps is None:
        return []
    data = []
    correl = []
    bpm = 0
    n = 0;
    nsamps = len(samps)
    window_samps = int(window * fs)
    samps_ndx = 0  # first sample in window_ndx
    max_window_ndx = int(nsamps / window_samps)
    bpms = numpy.zeros(max_window_ndx)

    # iterate through all windows
    for window_ndx in range(0, max_window_ndx):

        # get a new set of samples
        # print n,":",len(bpms),":",max_window_ndx,":",fs,":",nsamps,":",samps_ndx
        data = samps[samps_ndx:samps_ndx + window_samps]
        if not ((len(data) % window_samps) == 0):
            raise AssertionError(str(len(data)))

        try:
            bpm, correl_temp = bpm_detector(data, fs)
        except:
            bpm, correl_temp = None, None

        if bpm == None:
            continue

        bpms[window_ndx] = bpm
        correl = correl_temp

        # iterate at the end of the loop
        samps_ndx = samps_ndx + window_samps;
        n = n + 1;  # counter for debug...

    # bpm = numpy.median(bpms)

    #print('Completed.  Estimated Beats Per Minute:', bpm)

    # plt.figure()
    # plt.hist(bpms)
    # plt.show()

    #n = range(0, len(correl))
    #plt.plot(n, abs(correl));
    #plt.show();  # plot non-blocking
    #time.sleep(10);
    #plt.close();
    return list(bpms)

def calculate_bpm_multiple_window(fn, window_min, window_max):
    bpms = []
    for window in range(window_min, window_max+1):
        bpms += calculate_bpm_single_window(fn, window)

    try:
        bpm_median = numpy.median(bpms)
        bpms_round = numpy.round(bpms)
        counter = Counter(bpms_round)
        most_common_bpm = counter.most_common(2)
        bpm_first_common = most_common_bpm[0][0]
        bpm_second_common = most_common_bpm[1][0]
        if bpm_first_common < 10:
            most_common_bpm = counter.most_common(3)
            bpm_first_common = most_common_bpm[1][0]
            bpm_second_common = most_common_bpm[2][0]

    except:
        print("Error in finding BPM of", fn)
        return None, None, None

    if not bpm_median and not bpm_first_common and not bpm_second_common:
        print("Error in finding BPM of", fn)
    return bpm_median, bpm_first_common, bpm_second_common

## Main program ##

# Open a csv and write the title
f = open('song_bpm.csv', mode = 'w')
f.write("Name of Song,Type of Dance,BPM Median, BPM Most Common, BPM Second Most Common\n")

# Open all wav songs and calculate the bpm and write to the csv file
list_of_dir = os.listdir("../music_sample_wav/")
if ".DS_Store" in list_of_dir:
    list_of_dir.remove(".DS_Store")
for dir in tqdm(list_of_dir):
    folder_path = "../music_sample_wav/" + dir
    file_list = os.listdir(folder_path)
    for file in file_list:
        file_path = folder_path + "/" + file
        bpm_median, bpm_first_common, bpm_second_common = calculate_bpm_multiple_window(file_path, window_min=3, window_max=10)
        f.write(file + "," + dir + "," + str(bpm_median) + "," + str(bpm_first_common) + "," + str(bpm_second_common) + "\n")
        print("The bpm of " + file + " are: " + str(bpm_median), str(bpm_first_common), str(bpm_second_common))

f.close()