import librosa
from tqdm import tqdm, tqdm_gui
import os

tqdm.monitor_interval = 0

# data, sr = librosa.load("/Users/noemievoss/ballroommusicclassify/music_sample_wav/waltz/christinaperriathousandyears (1).mp3.wav")
# tempo, beats = librosa.beat.beat_track(y=data, sr=sr)
# plt.figure()
# plt.plot(data)
# plt.show()
# plt.figure()
# plt.plot(data[beats])
# plt.show()

## Main program ##

# Open a csv and write the title
f = open('song_bpm_librosa.csv', mode = 'w')
f.write("Name of Song,Type of Dance,BPM\n")

# Open all wav songs and calculate the bpm and write to the csv file
list_of_dir = os.listdir("../music_sample_wav/")
if ".DS_Store" in list_of_dir:
    list_of_dir.remove(".DS_Store")

for dir in tqdm(list_of_dir):
    folder_path = "../music_sample_wav/" + dir
    file_list = os.listdir(folder_path)
    for file in tqdm_gui(file_list):
        file_path = folder_path + "/" + file
        try:
            data, sr = librosa.load(file_path)
            tempo, beats = librosa.beat.beat_track(y=data, sr=sr)
            f.write(file + "," + dir + "," + str(tempo) + "\n")
            print("The bpm of " + file + " is: " + str(tempo))
        except:
            print("error in file:",file)

f.close()