import pandas as pd
from pydub import AudioSegment
import os
import madmom
import time
import os
from tqdm import tqdm
import numpy as np
import librosa
from scipy.fftpack import fft
from pyAudioAnalysis import audioFeatureExtraction
import pickle
from keras.models import load_model
tqdm.monitor_interval = 0

# Constant
FIXED_WINDOWS_TO_CUT = 10 *1000 # cut 10 second windows
OVERLAP = 0 *1000 # define the 5 seconds overlap
RANGE = [30,40,80,120,180,300] # frequency range for fingerprint
# Name of Song,Type of Dance,Count, BPM, maxfreq_30_40, maxfreq_40_80, maxfreq_80_120, maxfreq_120_180, maxfreq_180_300
# Short-term features name
st_feature_names = ['zcr', 'energy', 'energy_entropy','spectral_centroid','spectral_spread','spectral_entropy',
'spectral_flux','spectral_rolloff','mfcc_1','mfcc_2','mfcc_3','mfcc_4','mfcc_5','mfcc_6','mfcc_7','mfcc_8','mfcc_9',
'mfcc_10','mfcc_11','mfcc_12','mfcc_13','chroma_1','chroma_2','chroma_3','chroma_4','chroma_5','chroma_6','chroma_7',
'chroma_8','chroma_9','chroma_10','chroma_11','chroma_12','chroma_std']
# scaler file
scaler_file = "../saved_models/x_scaler.pickle"
# modle file
svm_file = "../saved_models/svm.pickle"
knn_file = "../saved_models/knn.pickle"
neural_network_file = "../saved_models/neural_network.h5"

# Take a file name
folder = "/Users/noemievoss/ballroommusicclassify/music_sample_mp3/jive/"
filename = "Footloose.mp3"
file_in = folder + filename
tmp_folder = "../tmp"

def classify(tmp_folder, file_in):
    if os.path.isdir(tmp_folder):
        import shutil
        shutil.rmtree(tmp_folder)
        os.mkdir(tmp_folder)
        print("Created tmp folder!")
    else:
        os.mkdir(tmp_folder)
        print("Created tmp folder!")
    #######################################################################################################################

    filename = file_in.split("/")[-1]

    # Cut the file into 10 sencond-window
    sound = AudioSegment.from_mp3(file_in) # read the whole song
    # determine how many chunks you will cut the song into
    iterations = int(sound.duration_seconds*1000/(FIXED_WINDOWS_TO_CUT-OVERLAP))
    for i in range(iterations):
        start = i * (FIXED_WINDOWS_TO_CUT-OVERLAP) # determine the start and end
        end = start + FIXED_WINDOWS_TO_CUT
        song_10_seconds = sound[start:end]
        song_10_seconds.export(tmp_folder + "/" + filename + "_" + str(i) +".wav", format="wav")

    #######################################################################################################################

    # Open a csv and write the title
    f = open(tmp_folder+'/song_features_advanced.csv', mode = 'w')
    try:
        string_to_write = "Name of Song,Count, BPM, maxfreq_30_40, maxfreq_40_80, maxfreq_80_120, maxfreq_120_180, maxfreq_180_300"
        for name in st_feature_names:
            column_name_mean = "mean_" + name
            column_name_std = "std_" + name
            string_to_write = string_to_write + ", " + column_name_mean + ", " + column_name_std

        f.write(string_to_write + "\n")


        folder_path = tmp_folder
        file_list = os.listdir(folder_path)
        for file in tqdm(file_list):
            file_path = folder_path + "/" + file
            try:
                ###### Count extraction #####
                proc = madmom.features.DBNDownBeatTrackingProcessor(beats_per_bar=[2, 3, 4], fps=100)
                act = madmom.features.RNNDownBeatProcessor()(file_path)
                count = proc(act)
                max_count = int(count[:,1].max())
                #print("Successfully extracted count feature")

                ##### BPM extraction ######
                data, sr = librosa.load(file_path)
                tempo, beats = librosa.beat.beat_track(y=data, sr=sr)
                #print("Successfully extracted bpm feature")

                ##### Fingerprint extraction ######
                if data.ndim == 2:
                    data_sum = data[:, 0] + data[:, 1]
                else:
                    data_sum = data
                fft_out = fft(data_sum)
                fft_out = np.abs(fft_out)
                max_ranges = []
                for i in range(len(RANGE) - 1):
                    start = i
                    end = start + 1
                    start_freq = RANGE[start]
                    end_freq = RANGE[end]
                    max_range = start_freq + np.argmax(fft_out[start_freq:end_freq])
                    max_ranges.append(max_range)

                ##### Short-term features and statistic of it (Mid-term) ######
                # Extract short-term features
                F, f_names = audioFeatureExtraction.stFeatureExtraction(data, sr, 0.050 * sr, 0.025 * sr)
                # Extract the stats of short-term features
                mean_st_features = F.mean(axis=1).reshape(-1)
                std_st_features = F.std(axis=1).reshape(-1)

                ##### Write to file #######

                string_to_write = '"'+ file + '"' + "," + str(max_count) + "," + str(tempo) + \
                                  "," + str(max_ranges[0]) + "," + str(max_ranges[1]) + "," + \
                                  str(max_ranges[2]) + "," + str(max_ranges[3]) + "," + str(max_ranges[4])

                for index, name in enumerate(f_names):
                    string_to_write = string_to_write + \
                                      "," + str(mean_st_features[index]) + \
                                      "," + str(mean_st_features[index])

                f.write(string_to_write + "\n")

            except Exception as e:
                print("error in file:",file, "\n" + str(e))
    except:
        f.close()

    finally:
        f.close()

    #######################################################################################################################

    # read the song_features file
    fn = tmp_folder + "/song_features_advanced.csv"
    df = pd.read_csv(fn)
    # subset the features data
    features = list(df.columns[1:])
    X_test = df[features]

    # load the scaler
    with open(scaler_file, 'rb') as f:
        scaler = pickle.load(f)

    X_test = scaler.transform(X_test)

    # load the svm model
    with open(svm_file, 'rb') as f:
        svm = pickle.load(f)

    # load the knn model
    with open(knn_file, 'rb') as f:
        knn = pickle.load(f)

    # load the nn model
    nn_model = model = load_model(neural_network_file)

    # Make the prediction
    y_pred_svm = svm.predict(X_test)

    # make knn results
    y_pred_knn = knn.predict(X_test)

    # make nn results
    y_pred_nn = model.predict(X_test)
    uniques = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    # Convert y_pred from One-hot to integer
    y_pred_nn = uniques[y_pred_nn.argmax(axis = 1)]


    # convert y_pred and y_test back into labels
    map_to_labels = {0: 'chachacha', 1: 'foxtrot_blues', 2: 'jive', 3: 'paso_doble', 4: 'quickstep',
                     5: 'rumba', 6: 'samba', 7: 'tango', 8: 'vienneze_waltz', 9: 'waltz'}
    y_pred_labels_svm = pd.Series(y_pred_svm).replace(map_to_labels)
    y_pred_labels_knn = pd.Series(y_pred_knn).replace(map_to_labels)
    y_pred_labels_nn = pd.Series(y_pred_nn).replace(map_to_labels)

    print("###################### SVM ######################")
    print(y_pred_labels_svm)
    print(y_pred_labels_svm.value_counts())
    print("###################### KNN ######################")
    print(y_pred_labels_knn)
    print(y_pred_labels_knn.value_counts())
    print("###################### NN ######################")
    print(y_pred_labels_nn)
    print(y_pred_labels_nn.value_counts())

classify(file_in=file_in, tmp_folder=tmp_folder)