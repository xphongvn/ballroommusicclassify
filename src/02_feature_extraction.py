import madmom
import time
import os
from tqdm import tqdm, tqdm_gui
import numpy as np
import librosa
from scipy.fftpack import fft

tqdm.monitor_interval = 0
RANGE = [30,40,80,120,180,300] # frequency range for fingerprint

# Name of Song,Type of Dance,Count, BPM, maxfreq_30_40, maxfreq_40_80, maxfreq_80_120, maxfreq_120_180, maxfreq_180_300

## Main program ##

# Open a csv and write the title
f = open('song_features.csv', mode = 'w')
f.write("Name of Song,Type of Dance,Count, BPM, maxfreq_30_40, maxfreq_40_80, maxfreq_80_120, maxfreq_120_180, maxfreq_180_300\n")

# Open all wav songs and calculate the bpm and write to the csv file
list_of_dir = os.listdir("../music_sample_chunks_train/")
if ".DS_Store" in list_of_dir:
    list_of_dir.remove(".DS_Store")

for dir in tqdm(list_of_dir):
    folder_path = "../music_sample_chunks_train/" + dir
    file_list = os.listdir(folder_path)
    for file in tqdm(file_list):
        file_path = folder_path + "/" + file
        try:
            ###### Count extraction #####
            proc = madmom.features.DBNDownBeatTrackingProcessor(beats_per_bar=[2, 3, 4], fps=100)
            act = madmom.features.RNNDownBeatProcessor()(file_path)
            count = proc(act)
            max_count = int(count[:,1].max())
            #print("Successfully extracted count feature")

            ##### BPM extraction ######
            data, sr = librosa.load(file_path)
            tempo, beats = librosa.beat.beat_track(y=data, sr=sr)
            #print("Successfully extracted bpm feature")

            ##### Fingerprint extraction ######
            if data.ndim == 2:
                data_sum = data[:, 0] + data[:, 1]
            else:
                data_sum = data
            fft_out = fft(data_sum)
            fft_out = np.abs(fft_out)
            max_ranges = []
            for i in range(len(RANGE) - 1):
                start = i
                end = start + 1
                start_freq = RANGE[start]
                end_freq = RANGE[end]
                max_range = start_freq + np.argmax(fft_out[start_freq:end_freq])
                max_ranges.append(max_range)
                #print("Successfully extracted fingerprint feature from {0} frequency to {1} frequency".format(start_freq,end_freq))

            ##### Write to file #######

            f.write('"'+ file + '"' + "," + dir + "," + str(max_count) + "," + str(tempo) + "," + str(max_ranges[0]) + "," + str(max_ranges[1]) + "," +
                    str(max_ranges[2]) + "," + str(max_ranges[3]) + "," + str(max_ranges[4]) + "\n")
            #print(file + "," + dir + "," + str(max_count) + "," + str(tempo) + "," + str(max_ranges[0]) + "," + str(max_ranges[1]) + "," +
            #        str(max_ranges[2]) + "," + str(max_ranges[3]) + "," + str(max_ranges[4]) + "\n")
        except:
            print("error in file:",file)

f.close()