import librosa
import matplotlib.pyplot as plt
from tqdm import tqdm, tqdm_gui
import numpy as np

tqdm.monitor_interval = 0

data, sr = librosa.load("/Users/noemievoss/ballroommusicclassify/music_sample_wav/waltz/christinaperriathousandyears (1).mp3.wav")
tempo, beats = librosa.beat.beat_track(y=data, sr=sr)

onset_env = librosa.onset.onset_strength(y=data, sr=sr, aggregate=np.median)
hop_length = 512
times = librosa.frames_to_time(np.arange(len(onset_env)), sr=sr, hop_length=hop_length)

plt.figure(figsize=(8, 4))
plt.plot(times, librosa.util.normalize(onset_env), label='Onset strength')
plt.vlines(times[beats], 0, 1, alpha=0.5, color='r', linestyle='--', label='Beats')
plt.legend(frameon=True, framealpha=0.75)
plt.xlim(50, 80)
plt.tight_layout()

###########################################################################################

