import madmom
import os
from tqdm import tqdm, tqdm_gui
import numpy as np
from madmom.models import PATTERNS_BALLROOM
from madmom.audio.spectrogram import LogarithmicSpectrogramProcessor, SpectrogramDifferenceProcessor, MultiBandSpectrogramProcessor
from madmom.processors import SequentialProcessor

tqdm.monitor_interval = 0

# Name of Song, Type of Dance, Count (3 or 4)

## Main program ##

# Open a csv and write the title
f = open('song_ballroom_count_madmom.csv', mode = 'w')
f.write("Name of Song,Type of Dance,Max Count, Way to Count\n")

# Open all wav songs and calculate the bpm and write to the csv file
list_of_dir = os.listdir("../music_sample_wav/")
if ".DS_Store" in list_of_dir:
    list_of_dir.remove(".DS_Store")

for dir in tqdm(list_of_dir):
    folder_path = "../music_sample_wav/" + dir
    file_list = os.listdir(folder_path)
    for file in tqdm(file_list):
        file_path = folder_path + "/" + file
        try:
            proc = madmom.features.PatternTrackingProcessor(PATTERNS_BALLROOM, fps=50)
            log = LogarithmicSpectrogramProcessor()
            diff = SpectrogramDifferenceProcessor(positive_diffs=True)
            mb = MultiBandSpectrogramProcessor(crossover_frequencies=[270])
            pre_proc = SequentialProcessor([log, diff, mb])
            act = pre_proc(file_path)
            proc(act)
            count = proc(act)
            max_count = int(count[:,1].max())
            way_to_count = count[:max_count,1]
            f.write(file + "," + dir + "," + str(max_count) + "," + str(way_to_count) + "\n")
            print("The Count of " + file + " is: " + str(np.unique(count[:,1])))
        except:
            print("error in file:",file)

f.close()