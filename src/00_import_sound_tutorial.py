# We will learn about this next week
import soundfile as sf
import numpy as np
import matplotlib.pyplot as plt
from pydub import AudioSegment
import pygame


filename = "../music_sample_mp3/samba/Shakira_-_Whenever_Wherever_The_Sun_Comes_Out_Live_at_Europe_(mp3.pm).mp3"
sound = AudioSegment.from_mp3(filename)
sound.export(filename+".wav", format="wav")

filename += ".wav"
signal, sample_rate = sf.read(filename)
plt.plot(signal)
signal.shape
signal.shape[0]/sample_rate

pygame.init()
pygame.mixer.music.load(filename)
pygame.mixer.music.play()
pygame.mixer.music.stop()

