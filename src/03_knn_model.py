import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler

# read the song_features file
fn = "/Users/noemievoss/ballroommusicclassify/src/song_features.csv"
df = pd.read_csv(fn)

# convert types of dance into unique integer numbers in order to classify
targets = df["Type of Dance"].unique()
map_to_integer = {dance:n for n, dance in enumerate(targets)}
map_to_labels = {n:dance for n, dance in enumerate(targets)}
df["targets"] = df["Type of Dance"].replace(map_to_integer)

# subset the features data
features = list(df.columns[2:9])

# split data into train and test data
X_train, X_test, y_train, y_test = train_test_split(df[features], df["targets"], test_size=.2, random_state=100)

# scale the data (normalise the data into the range 0-1)
scaler = StandardScaler() # initialise the scaler
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# build the model
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)

# evaluate the model
y_pred = knn.predict(X_test)
# convert y_pred and y_test back into labels
y_test_labels = y_test.replace(map_to_labels)
y_pred_labels = pd.Series(y_pred).replace(map_to_labels)

score = metrics.classification_report(y_test, y_pred)
print(score)

plt.figure()
mat = metrics.confusion_matrix(y_test, y_pred)
sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False,
            xticklabels=targets, yticklabels=targets)
plt.xlabel('true label')
plt.ylabel('predicted label')
plt.title('Result of KNN Model (neighbours=10)')
plt.show()